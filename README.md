# Protein Adjacency Net

[![gitlab](https://img.shields.io/badge/GitLab-main-orange?logo=gitlab)](https://gitlab.com/ostrokach/protein-adjacency-net)
[![docs](https://img.shields.io/badge/docs-v0.1.14-blue.svg?logo=gitbook)](https://ostrokach.gitlab.io/protein-adjacency-net/v0.1.14/)
[![conda](https://img.shields.io/conda/dn/kimlab/protein-adjacency-net.svg?logo=conda-forge)](https://anaconda.org/kimlab/protein-adjacency-net/)
[![pipeline status](https://gitlab.com/ostrokach/protein-adjacency-net/badges/v0.1.14/pipeline.svg)](https://gitlab.com/ostrokach/protein-adjacency-net/commits/v0.1.14/)
[![coverage report](https://gitlab.com/ostrokach/protein-adjacency-net/badges/master/coverage.svg?job=docs)](https://ostrokach.gitlab.io/protein-adjacency-net/v0.1.14/htmlcov/)

Protein Adjacency Net (PAN) is a graph neural network trained to predict whether a given protein sequence matches a given protein adjacency matrix. It is the predecessor of the more successful [ProteinSolver]() project

## Notebooks2 (aka adjacency-net-v2)

- [`04_validation_demo_dataset.ipynb`](notebooks/04_validation_demo_dataset.ipynb) — Check if we are able to make predictions for a simple demo dataset.
- [`04_validation_protherm_dataset.ipynb`](notebooks/04_validation_protherm_dataset.ipynb) — Use trained network to predictict mutation ΔΔG.

## Notebooks (aka adjacency-net)

